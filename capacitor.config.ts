import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'test-oden',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
