import { Component } from '@angular/core';
import { ContactModel, contactI } from './shared/contact';
import { SharedService } from './shared/services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor( private router: Router) {
  }
  goHome(){
    this.router.navigateByUrl('/home')
  }
  goContact(){
    this.router.navigateByUrl('/contact')
  }

}
