import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-type-user',
  templateUrl: './type-user.component.html',
  styleUrls: ['./type-user.component.scss'],
})
export class TypeUserComponent  implements OnInit {
  @Input() userData:any;
  
  @Output() userDataOutput = new EventEmitter<any>();
  constructor() {
   }

  ngOnInit() {
  }
  setTypeUser(e:any){
    this.userData.type = e.detail.value;
    localStorage.setItem('userInfo', JSON.stringify(this.userData));
    this.userDataOutput.emit(this.userData);
  }
}
