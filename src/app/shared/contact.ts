export interface contactI {
    email:string ,
    phone: string,
    instagram: string,
    facebook: string,
    youtube: string
}
export class ContactModel {
    public email = 'hey@oden.mx';
    public phone = '479 145 8069';
    public instagram = '@oden.fc';
    public facebook = 'Oden';
    public youtube = 'ODEN Fitness Control'
}