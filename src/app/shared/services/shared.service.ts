import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { contactI } from '../contact';

@Injectable({providedIn: 'root'})
export class SharedService {
    public contactInformationObs:BehaviorSubject<contactI> = new BehaviorSubject(
        {
             email : 'hey@oden.mx',
             phone : '479 145 8069',
             instagram : '@oden.fc',
             facebook : 'Oden',
             youtube : 'ODEN Fitness Control'
        }
    );
    constructor() { }
    
}