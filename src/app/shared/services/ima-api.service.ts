import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMAAPPI } from '../ima-api';

@Injectable({providedIn: 'root'})
export class ImaService {
    constructor(private httpClient: HttpClient) { }
    
    public getImageAndInformation(){
        
        return this.httpClient.get(IMAAPPI.api+'?key='+IMAAPPI.token+'&q=sports&image_type=photo&orientation=horizontal&pretty=true&safesearch=true&min_width='+IMAAPPI.options.min_width+'&min_height='+IMAAPPI.options.min_height+'&per_page=21')
    }
}