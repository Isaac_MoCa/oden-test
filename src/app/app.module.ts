import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ImaService } from './shared/services/ima-api.service';
import { HttpClientModule} from '@angular/common/http';
import { HomePageModule } from './home/home.module';
import { ContactPageModule } from './contact/contact.module';

@NgModule({
  declarations: [AppComponent,],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, HomePageModule, ContactPageModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, ImaService],
  bootstrap: [AppComponent],
})
export class AppModule {}
