import { Component } from '@angular/core';
import { ImaService } from '../shared/services/ima-api.service';
import { ContactModel, contactI } from '../shared/contact';
import { SharedService } from '../shared/services/shared.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public data:Array<any> = [];
  public contact: contactI = new ContactModel();
  constructor(
    private api:ImaService,
    private shared: SharedService
  ) {
    this.shared.contactInformationObs.subscribe((response ) => {
        this.contact = response;
    })
  }

  ngOnInit(): void {
    let storage:any = localStorage.getItem('contactStorage');
    this.contact = JSON.parse(storage) ? JSON.parse(storage) : this.contact;
    this.api.getImageAndInformation().subscribe((response:any)=>{
      this.data = response.hits;
    })
    
  }
  ngOnDestroy(): void {
  }
}
