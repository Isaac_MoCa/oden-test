import { Component, OnInit } from '@angular/core';
import { ContactModel, contactI } from '../shared/contact';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { SharedService } from '../shared/services/shared.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  public userData:any;
  public contactForm: FormGroup = new FormGroup({});
  public contact:any;

  constructor(private toastController:ToastController, private shared: SharedService ) { 
    this.shared.contactInformationObs.subscribe((response ) =>{
      this.contact = response;
    }).unsubscribe();
    let data:any =  localStorage.getItem('userInfo');
    this.userData = JSON.parse(data) ? JSON.parse(data) : {
      type: 'admin',
      name: 'Isaac Morales',
      isActive: true
    };
    
  }

  ngOnInit() {
    let storage:any = localStorage.getItem('contactStorage');
    this.contact = JSON.parse(storage) ? JSON.parse(storage) : this.contact;
    this.doForm();
  }
  saveForm(){
    this.shared.contactInformationObs.next(this.contactForm.value);
    this.contact = this.contactForm.value;
    localStorage.setItem('contactStorage', JSON.stringify(this.contactForm.value));
    this.presentToast();
  }
  private doForm(){
    console.log(this.userData);
    var isDisable : boolean = this.userData.type === 'admin' ? false : true;
    this.contactForm = new FormGroup({
      email : new FormControl({value: this.contact.email, disabled:isDisable}, [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      phone: new FormControl({value: this.contact.phone, disabled:isDisable}, [Validators.required, ]),
      instagram: new FormControl({value: this.contact.instagram, disabled:isDisable}, [Validators.required, ]),
      facebook: new FormControl({value: this.contact.facebook, disabled:isDisable}, [Validators.required, ]),
      youtube: new FormControl({value: this.contact.youtube, disabled:isDisable}, [Validators.required, ])
    })
  }
  private async presentToast() {
    const toast = await this.toastController.create({
      message: 'Cambios Guardados',
      duration: 1500,
      position: 'bottom',
      color:'success'
    });

    await toast.present();
  }
  checkValidators() {
    if (this.contactForm.status == 'INVALID') {
      return true
    } else {
      return false
    }
  }
  setUserData(event:any){
    this.userData = event;
  }

}
